data "ignition_systemd_unit" "install_vmware_tools" {
  name = "rpm-ostree-install-open-vm-tools.service"
  enabled = true
  content = file("${path.module}/templates/vmtoolsd.service")
}

data "ignition_systemd_unit" "vmtoolsd" {
  name = "vmtoolsd.service"
  enabled = true
}

data "ignition_config" "config" {
  systemd = [
    data.ignition_systemd_unit.install_vmware_tools.rendered,
    data.ignition_systemd_unit.vmtoolsd.rendered,
  ]
}
